const drawSunClock = (ctx, sunrise, sunset, startX, endX, startY, endY) => {
    const borderWidth = 10;

    // Nice on colour screen
    const borderColour1 = '#cce4fe';
    const borderColour2 = '#fee4cc';
    const centreColour = '#eee';
    const sunArcColour = '#ccc';

    const hourHandColour = '#000';
    const midnightColour = '#111111';

    // Calculate the angles for drawing
    const sunRiseAngle = getAngle(sunrise);
    const sunSetAngle = getAngle(sunset);
    const currentAngle = getAngle(new Date());

    // Draw the outer ring (actually just a series of segments, we'll overdraw them next)
    drawBorder(ctx, borderColour1, borderColour2);
    // Hollow out the centre
    drawCentre(ctx, centreColour, borderWidth);

    // Sunrise - arc from bottom dead centre to rise hour
    drawSunArc(ctx, borderWidth, sunArcColour, sunRiseAngle, false);

    // Sunset - arc from bottom dead centre to set hour, anticlockwise
    drawSunArc(ctx, borderWidth, sunArcColour, sunSetAngle, true);

    // Now - the hour hand
    drawLine(ctx, currentAngle, hourHandColour, borderWidth);

    ctx.font = Math.floor(canvas.height / 25) + 'pt Arial';
    ctx.fillStyle = 'black';

    const riseText =
        '\u25b2' +
        `${twoDigit(sunrise.getHours())}:${twoDigit(sunrise.getMinutes())}`;
    ctx.textAlign = 'left';

    ctx.fillText(riseText, 0, canvas.height);
    const setText =
        `${twoDigit(sunset.getHours())}:${twoDigit(sunset.getMinutes())}` +
        '\u25bc';
    ctx.textAlign = 'right';

    ctx.fillText(setText, canvas.width, canvas.height);
};

// Return a number as two digits, adding a leading 0 if required
// Always returns a string for consistency
const twoDigit = (number) => {
    return parseInt(number) < 10 ? '0' + parseInt(number) : number.toString();
};

// Takes a date object and returns the angle it would describe if a 24 hour day was a circle
// Radians
const getAngle = (myDate) => {
    // Minutes / 1440 minutes in a 24h day to get a fraction, then multiply * 2 x pi radians
    return (
        ((myDate.getHours() * 60 + myDate.getMinutes()) * 2 * Math.PI) / 1440
    );
};

// Draws a line to intercept a circle at the given angle
// Assumes 0 radians is straight down
const drawLine = (ctx, angle, colour, border) => {
    // Find the middle
    const canvasCentreX = canvas.width / 2 + 0;
    const canvasCentreY = canvas.height / 2 + 0;

    ctx.beginPath();
    ctx.moveTo(canvasCentreX, canvasCentreY);
    let length = canvas.height / 2 - border;

    ctx.arc(
        canvasCentreX,
        canvasCentreY,
        length,
        angle + Math.PI / 2,
        angle + Math.PI / 2
    );
    ctx.strokeStyle = colour;
    ctx.stroke();
    ctx.closePath();
};

// Draws a filled circle out to the maximum extents of the canvas
const drawBorder = (ctx, colour1, colour2) => {
    // Find the middle
    const canvasCentreX = canvas.width / 2;
    const canvasCentreY = canvas.height / 2;

    for (var i = 0; i < 24; i++) {
        ctx.beginPath();

        ctx.moveTo(canvasCentreX, canvasCentreY);

        ctx.arc(
            canvasCentreX,
            canvasCentreY,
            canvas.height / 2,
            (i * (Math.PI * 2)) / 24,
            ((i + 1) * (Math.PI * 2)) / 24
        );
        ctx.strokeStyle = i % 2 ? colour1 : colour2;
        ctx.fillStyle = i % 2 ? colour1 : colour2;
        ctx.fill();
        ctx.stroke();
        ctx.closePath();
    }
};

// Draws the centre area of the circle, in by 'border' amount to leave the border
const drawCentre = (ctx, colour, border) => {
    // Find the middle
    const canvasCentreX = canvas.width / 2 + 0;
    const canvasCentreY = canvas.height / 2 + 0;

    ctx.beginPath();
    ctx.arc(
        canvasCentreX,
        canvasCentreY,
        canvas.height / 2 - border,
        0,
        Math.PI * 2
    );
    ctx.strokeStyle = colour;
    ctx.fillStyle = colour;
    ctx.fill();
    ctx.stroke();
    ctx.closePath();
};

// Draws an arc to represent sunrise or sunset, and fills it
// set = true ensure it's drawn in the right place and
const drawSunArc = (ctx, border, colour, angle, set = false) => {
    // Find the middle
    const canvasCentreX = canvas.width / 2 + 0;
    const canvasCentreY = canvas.height / 2 + 0;

    ctx.beginPath();
    ctx.moveTo(canvasCentreX, canvasCentreY);
    ctx.lineTo(canvasCentreX, canvas.height); // Straight to the bottom - 00:00

    ctx.arc(
        canvasCentreX,
        canvasCentreY,
        canvas.height / 2 - border,
        Math.PI / 2,
        angle + Math.PI / 2,
        set
    );
    ctx.strokeStyle = colour;
    ctx.fillStyle = colour;
    ctx.fill();
    ctx.stroke();
    ctx.closePath();
};

const prettyLocation = (lat, lon) => {
    if (lat > 0) {
        latDeg = Math.floor(lat);
        latHemi = 'N';
        latMin = Math.floor((lat - latDeg) * 60);
        latSec = Math.floor((lat - latDeg - latMin / 60) * 60 * 60);
    } else {
        latDeg = 0 - Math.ceil(lat);
        latHemi = 'S';
        latMin = Math.floor((0 - lat - latDeg) * 60);
        latSec = Math.floor((0 - lat - latDeg - latMin / 60) * 60 * 60);
    }

    if (lon > 0) {
        lonDeg = Math.floor(lon);
        lonHemi = 'E';
        lonMin = Math.floor((lon - lonDeg) * 60);
        lonSec = Math.floor((lon - lonDeg - lonMin / 60) * 60 * 60);
    } else {
        lonDeg = 0 - Math.ceil(lon);
        lonHemi = 'W';
        lonMin = Math.floor((0 - lon - lonDeg) * 60);
        lonSec = Math.floor((0 - lon - lonDeg - lonMin / 60) * 60 * 60);
    }

    return `lat: ${latDeg}&deg;, ${latMin}', ${latSec}" ${latHemi}, lon: ${lonDeg}&deg;, ${lonMin}', ${lonSec}" ${lonHemi}`;
};

const getLocation = () => {
    // Default location
    var lat = 52 + 22 / 60 + 20 / 3600;
    var lon = 0 - (2 + 35 / 60 + 58 / 3600);

    var cv = document.getElementById('canvas');
    var ctx = cv.getContext('2d');

    var location = document.getElementById('location');

    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(
            (position) => {
                lat = position.coords.latitude;
                lon = position.coords.longitude;

                location.innerHTML = prettyLocation(lat, lon);

                var { sunrise, sunset } = SunCalc.getTimes(
                    new Date(),
                    lat,
                    lon
                );
                drawSunClock(ctx, sunrise, sunset, 0, 100, 0, 100);
            },
            (error) => {
                console.log(error, 'Need access to get location.');
                location.innerHTML = `Default location since you disabled it. ${prettyLocation(
                    lat,
                    lon
                )}`;
                var { sunrise, sunset } = SunCalc.getTimes(
                    new Date(),
                    lat,
                    lon
                );
                drawSunClock(ctx, sunrise, sunset, 0, 100, 0, 100);
            }
        );
    }
};

getLocation();
setInterval(getLocation, 60000); // Interval set to 60 seconds
