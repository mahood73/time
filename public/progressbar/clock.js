var myYearDiv = document.getElementById('yearProgress');
var myDayDiv = document.getElementById('dayProgress');
var myTimeDiv = document.getElementById('currentTime');

// milliseconds in a day
const oneDay = 1000 * 60 * 60 * 24;

const updateYear = () => {
    // Where are we now
    var timeNow = new Date();

    // First day of this year
    const startYear = new Date(timeNow.getFullYear(), 0, 0);

    // How many milliseconds into the year are we?
    const diffNow =
        timeNow -
        startYear +
        // Allowing for DST
        (startYear.getTimezoneOffset() - timeNow.getTimezoneOffset()) *
            60 *
            1000;

    // Today's day number
    const day = Math.floor(diffNow / oneDay);

    // First day of next year gives us number of days in this year
    const timeEnd = new Date(timeNow.getFullYear() + 1, 0, 0);

    // How many milliseconds into the year is the end?
    const diffEnd = timeEnd - startYear;
    // No need for DST

    const daysInYear = Math.floor(diffEnd / oneDay);

    const yearPercent = (100 * day) / daysInYear;

    myYearDiv.innerHTML = `<div class="w3-light-grey"><div class="w3-blue w3-center" style="height:24px;width:${yearPercent}%"></div></div>`;
};

const updateDay = () => {
    // Where are we now
    var timeNow = new Date();

    // First ms of this day
    const startDay = timeNow - (timeNow % oneDay);

    // How far through the day are we
    const dayPercent = (100 * (timeNow - startDay)) / oneDay;

    myDayDiv.innerHTML = `<div class="w3-light-grey"><div class="w3-blue w3-center" style="height:24px;width:${dayPercent}%"></div></div>`;
    myTimeDiv.innerHTML = timeNow;
};

// Set them immediately
updateDay();
updateYear();

// Update the year each hour, for now
setInterval(updateYear, 1000 * 60);

// Once a second for the day
setInterval(updateDay, 1000);
